----------------------------------------------------------------------------------
-- archivo:       top.vhd
-- descripcion:   top level del proyecto loopback Atlys Spartan 6.
-- board:         Atlys (Digilent)
-- fpga:          Spartan6 XC6SLX45 CSG324C (Xilinx)
-- entradas:         clk,rst_l               clock y reset
--                   sdin,bclk               codec
--                   volume,mode             control
-- salidas:          sdout,sync,ac97_rst_l   codec
--                   led                     status
--
-- autor:         agustin diaz antuna
-- contacto:      agustin.diazantuna@gmail.com
-- fecha:         24-06-2016
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity top is
   port (   clk         : in  std_logic;
            rst_l       : in  std_logic;
            -- codec ports
            sdin        : in std_logic;
            sdout       : out std_logic;
            sync        : out std_logic;
            bclk        : in std_logic;
            ac97_rst_l  : out std_logic;
            -- control ports
            volume      : in std_logic_vector(4 downto 0);
            mode        : in std_logic;
            -- led status
            led         : out std_logic_vector(7 downto 0)
         );
end top;


architecture behavioral of top is

   -------------------------------------------------------------------------------
   -- component
   -------------------------------------------------------------------------------


   -- constants
   

   -- signals
   -- codec audio: ac97
   signal latching_cmd : std_logic;
   signal ready : std_logic;
   signal sig_din_chL, sig_din_chR : std_logic_vector(17 downto 0);
   signal sig_dout_chL, sig_dout_chR : std_logic_vector(17 downto 0);

   signal cmd_addr : std_logic_vector(7 downto 0);
   signal cmd_data : std_logic_vector(15 downto 0);

   -- codec audio: ac97cmd
   signal sig_source : std_logic_vector(2 downto 0) := "100";
   
   
begin

   -- instantiation     
   ac97_inst : entity work.ac97(arch)
      port map(n_reset => rst_l,
               clk => clk,
               ac97_sdata_out => sdout, 
               ac97_sdata_in => sdin, 
               latching_cmd => latching_cmd ,
               ac97_sync => sync,
               ac97_bitclk => bclk,
               ac97_n_reset => ac97_rst_l,
               ac97_ready_sig => ready,
               l_out => sig_dout_chL,
               r_out => sig_dout_chR,
               l_in => sig_din_chL,
               r_in => sig_din_chR,
               cmd_addr => cmd_addr,
               cmd_data => cmd_data
               );
 
   ac97cmd_inst : entity work.ac97cmd(arch)
      port map (clk => clk, 
                ac97_ready_sig => ready,
                cmd_addr => cmd_addr,
                cmd_data => cmd_data,
                volume => volume, 
                source => sig_source, 
                latching_cmd => latching_cmd
                );  
                

--   sig_rst <= not rst_l;
   led(4 downto 0) <= volume;
   led(6 downto 5) <= (others => '0');
    

   process (clk, rst_l, mode, sig_din_chL, sig_dout_chR)
   begin 
      if (clk'event and clk = '1') then
         if rst_l = '0' then
            sig_dout_chL <= (others => '0');
            sig_dout_chR <= (others => '0');
            led(7) <= '0';
         elsif(ready = '1') then
            if mode = '1' then
               -- insert here your dsp
               sig_dout_chL <= (others => '0');
               sig_dout_chR <= (others => '0');
               led(7) <= '1';
            else
               -- loopback default
               sig_dout_chL <= sig_din_chL;
               sig_dout_chR <= sig_din_chR;
               led(7) <= '0';
            end if;
         end if;
      end if;
   end process;

end architecture;



