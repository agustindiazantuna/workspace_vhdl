--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:47:12 05/25/2016
-- Design Name:   
-- Module Name:   /home/agustin/Dropbox/FPGA_FIR_FILTER/fir_filter/fir_tb_iofiles.vhd
-- Project Name:  fir_filter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: fir
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

-- manejo de archivos 
use std.textio.all ; 
use work.myPackage_IO.all;			-- tomar datos
use ieee.std_logic_textio.all;	-- guardar datos

ENTITY fir_tb_iofiles IS
END fir_tb_iofiles;
 
ARCHITECTURE behavior OF fir_tb_iofiles IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT fir
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         data_in : IN  std_logic;
         data_out : OUT  std_logic;
         filter_time : OUT  std_logic;
         x : IN  std_logic_vector(15 downto 0);
         y : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal sig_clk : std_logic := '0';
   signal sig_rst : std_logic := '0';
   signal sig_data_in : std_logic := '0';
   signal sig_x : std_logic_vector(15 downto 0) := (others => '0');

 	--Outputs
   signal sig_data_out : std_logic;
   signal sig_filter_time : std_logic;
   signal sig_y : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk_period : time := 20 ns;	-- 50 MHz
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: fir PORT MAP (
          clk => sig_clk,
          rst => sig_rst,
          data_in => sig_data_in,
          data_out => sig_data_out,
          filter_time => sig_filter_time,
          x => sig_x,
          y => sig_y
        );

   -- Clock process definitions
   clk_process :process
   begin
		sig_clk <= '0';
		wait for clk_period/2;
		sig_clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process

		FILE fid_out: text open write_mode is "./data_out.txt";
		FILE fid_in : text open read_mode is "./data_in.txt";		
		
		variable vector_line_x : line;   		
		
		variable  test_x : std_logic_vector(15 downto 0);
		
		variable vector_line_y : line; -- para guardar la salida
    	 
	begin
		
		sig_rst <= '1';
		sig_data_in <= '0';
		sig_x <= (others => '0');
		wait for clk_period*2;
		sig_rst <= '0';
		
		while not endfile(fid_in) loop
			
			sig_data_in <= '1';
			
			-- Leo Operador X
			readline(fid_in,vector_line_x);    
			ReadFromLine_SLV(vector_line_x,test_x);
			sig_x <= test_x ;
						
			wait for clk_period;
			
			sig_data_in <= '0';
			
			wait for clk_period*60;
			
			write(vector_line_y, sig_y);			-- first value in row
			writeline(fid_out, vector_line_y);	-- write row to output file

		end loop;
	
		assert false
		report "end"
		severity failure;
		
   end process;

END;