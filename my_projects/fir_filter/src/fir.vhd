----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:51:41 11/15/2015 
-- Design Name: 
-- Module Name:    fir - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

entity fir is
	generic (W1 : INTEGER := 16;					-- Input bit width
				W2 : INTEGER := 32;					-- Multiplier bit width 2*W1
				W3 : INTEGER := 37;					-- Adder width = W2+log2(L)-1
				W4 : INTEGER := 16;					-- Output bit width
				L : INTEGER := 50;					-- Filter length (L downto 0)
				LEN : INTEGER := 5
				);					
    Port ( clk : in  STD_LOGIC;
			  rst : in STD_LOGIC;
			  data_in : in STD_LOGIC;
			  data_out : out STD_LOGIC;
			  filter_time : out STD_LOGIC;
           x : in  STD_LOGIC_VECTOR (W1-1 downto 0);
           y : out  STD_LOGIC_VECTOR (W4-1 downto 0);
			  
			  --SACO ROM
			  addra : OUT std_logic_vector(LEN downto 0);
			  douta : IN std_logic_vector(15 downto 0));
			  --
end fir;

architecture Behavioral of fir is

	COMPONENT binary_counter
	GENERIC(
		M : INTEGER
		);
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;          
		q : OUT std_logic_vector
		);
	END COMPONENT;
	
	COMPONENT counter
	GENERIC(
		M : INTEGER
		);
	PORT(
		clk : IN std_logic;
		rst : IN std_logic;          
		tc : OUT std_logic;
		half_tc : OUT std_logic
		);
	END COMPONENT;

	-- Constants
	
	CONSTANT N : INTEGER := integer(ceil(log2(real(L))));
	
	-- Types - Subtypes

	SUBTYPE N1BIT IS STD_LOGIC_VECTOR(W1-1 downto 0);
	SUBTYPE N2BIT IS STD_LOGIC_VECTOR(W2-1 downto 0);
	SUBTYPE N3BIT IS STD_LOGIC_VECTOR(W3-1 downto 0);

	TYPE ARRAY_N1BIT IS ARRAY (L+1 downto 0) OF N1BIT;				-- Un lugar extra por el ciclo de clock de retraso de la ROM

	-- Signals
	
	---- NUEVO
	SIGNAL regp_aux : N1BIT := (others => '0');									-- Signal data
	SIGNAL rega : N1BIT := (others => '0'); 										-- Adder array
	SIGNAL rega_aux : STD_LOGIC_VECTOR(W1 downto 0) := (others => '0'); 	-- Adder array
	---- NUEVO

	SIGNAL regx : ARRAY_N1BIT := (others => (others => '0'));				-- Data array
	SIGNAL regp : N2BIT := (others => '0');										-- Product array
--	SIGNAL rega : N3BIT := (others => '0'); 										-- Adder array

	SIGNAL regc_aux : N1BIT := (others => '0');									-- Signal coef
	SIGNAL regx_aux : N1BIT := (others => '0');									-- Signal data
--	SIGNAL rega_aux : N3BIT := (others => '0');									-- Signal adder
	SIGNAL reg_douta : N1BIT := (others => '0');									-- Signal douta

	SIGNAL clear : STD_LOGIC := '0';													-- Clear (p,a,y) when new data
	SIGNAL enable : STD_LOGIC := '0';												-- Filter time
	SIGNAL tick : STD_LOGIC := '0';
	SIGNAL count : STD_LOGIC_VECTOR (N-1 downto 0) := (others => '0');	-- Mux coef and data
	
	SIGNAL all_zeros : STD_LOGIC_VECTOR((W3-1)-W4 downto 0) := (others => '0');
	SIGNAL all_ones : STD_LOGIC_VECTOR((W3-1)-W4 downto 0) := (others => '1');

	
	
begin

	-- Asignations
	clear <= data_in;																		-- Clear p,a,y when new data
	filter_time <= enable;
	
	regx_aux <= regx(to_integer(L+1-unsigned(count)));
--	rega_aux <= regp(W2-1)&regp(W2-1)&regp(W2-1)&regp(W2-1)&regp(W2-1)&regp;
	regc_aux <= reg_douta;

	
	addra <= count;
	reg_douta <= douta;
	
	Inst_binary_counter: binary_counter				-- cuenta desde 0 hasta L+1 por el cero extra de la ROM
	GENERIC MAP(
		M => L + 1
	)
	PORT MAP(
		clk => clk,
		rst => data_in,
		q => count
	);
	
	Inst_counter: counter
	GENERIC MAP(
		M => L + 2
	)
	PORT MAP(
		clk => clk,
		rst => data_in,
		tc => tick,
		half_tc => open
	);

Load: process(clk)											-- Load data
begin
	if rising_edge(clk) then
		if rst = '1' then										-- Clean regx
			regx <= (others => (others => '0'));
		elsif data_in = '1' then							-- New data
			regx(L downto 0) <= x & regx(L downto 1);	-- Data shift one + retraso por ROM
		end if;
	end if; 
end process Load;

Multiplication: process(clk)														-- Multiplication p = x * c
begin
	if rising_edge(clk) then
		if rst = '1' or clear = '1' then											-- Clean regp
			regp <= (others => '0');
		elsif enable = '1' then
			regp <= std_logic_vector(signed(regx_aux) * signed(regc_aux));
			---------------- NUEVO
			if ( unsigned(regp(W2-1 downto W2-2)) = "00" or unsigned(regp(W2-1 downto W2-2)) = "11" ) then
				regp_aux <= std_logic_vector(regp(W2-2 downto W2-1-W1));
			elsif unsigned(regp(W2-1 downto W2-2)) = "01" then
				regp_aux <= (15 => '0' , others => '1' );
			else
				regp_aux <= (15 => '1' , others => '0' );
			end if;
			---------------- NUEVO
		end if;
	end if;
end process;

SOP : process(clk)																	-- Compute sum-of-products
begin
	if rising_edge(clk) then
		if rst = '1' or clear = '1' then											-- Clean rega
			rega <= (others => '0');
			rega_aux <= (others => '0');
		elsif enable = '1' then
--			rega <= std_logic_vector(signed(rega) + signed(rega_aux));	-- filter adds
			---------------- NUEVO
			rega_aux <= std_logic_vector(signed(regp_aux) + signed(rega_aux));
			if ( signed(rega_aux) < to_signed(65535,rega_aux'length) or signed(rega_aux) >= to_signed(-65535,rega_aux'length) ) then
				rega <= rega_aux(W1-1 downto 0);
			elsif ( signed(rega_aux) >= to_signed(65535,rega_aux'length) ) then
				rega_aux <=(15 => '0' , others => '1' );
			else
				rega_aux <= (15 => '1' , others => '0' );
			end if;
			---------------- NUEVO
		end if;
	end if;
end process SOP;

Output : process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' or clear = '1' then											-- Clean y
			y <= (others => '0');
			data_out <= '0';
		elsif tick = '1' and enable = '1' then
--			if unsigned(rega(W3-1 downto W4+14)) = unsigned(all_zeros) or unsigned(rega(W3-1 downto W4+14)) = unsigned(all_ones) then	-- Without overflow
--				y <= rega(29 downto 14);											-- Exit
--			elsif rega(W3-1) = '0' then											-- Overflow, y = 01111...
--				y <= (15 => '0' , others => '1' );
--			else																			-- Underflow, y = 10000...
--				y <= (15 => '1' , others => '0');
--			end if;
			---------------- NUEVO
			y <= rega;
			---------------- NUEVO
			data_out <= '1';
		else
			data_out <= '0';
		end if;
	end if;
end process;

en : process(clk)
begin
	if rising_edge(clk) then
		if data_in = '1' then
			enable <= '1';
		elsif tick = '1' then
			enable <= '0';
		end if;
	end if;
end process;

end Behavioral;
