----------------------------------------------------------------------------------
-- archivo:       top.vhd
-- descripcion:   top level del proyecto fir filter.
-- entradas:         clk,rst        clock y reset
-- salidas:          tc, half_tc    terminal count
--
-- autor:         agustin diaz antuna
-- contacto:      agustin.diazantuna@gmail.com
-- fecha:         22-06-2016
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity top is
   generic ( STAGES : integer := 5 );
   port (   clk            : in  std_logic;
            n_reset        : in  std_logic;
            mode           : in std_logic;
            filter_time    : out std_logic;
            sdata_in       : in std_logic;
            bit_clk        : in std_logic;
            volume         : in std_logic_vector(4 downto 0);
            sync           : out std_logic;
            sdata_out      : out std_logic;
            ac97_n_reset   : out std_logic;
            filtro         : in std_logic_vector(1 downto 0);
            led            : out std_logic_vector(7 downto 0)
         );
end top;

architecture behavioral of top is

   -------------------------------------------------------------------------------
   -- component
   -------------------------------------------------------------------------------
   component fir
      port( clk : in std_logic;
            rst : in std_logic;
            data_in : in std_logic;
            x : in std_logic_vector(15 downto 0);          
            data_out : out std_logic;
            filter_time : out std_logic;
            y : out std_logic_vector(15 downto 0);
            --saco rom
            addra : out std_logic_vector(STAGES downto 0);
            douta : in std_logic_vector(15 downto 0)
            --
         );
   end component;
   

   component blk_mem_gen_v7_3    -- multiband filter
   port(
      clka : in std_logic;
      addra : in std_logic_vector(STAGES downto 0);          
      douta : out std_logic_vector(15 downto 0)
      );
   end component;
   
   component blk_mem_gen_v7_1    -- lowpass filter
   port(
      clka : in std_logic;
      addra : in std_logic_vector(STAGES downto 0);          
      douta : out std_logic_vector(15 downto 0)
      );
   end component;
   
   component blk_mem_gen_v7_2    -- bandpass filter
   port(
      clka : in std_logic;
      addra : in std_logic_vector(STAGES downto 0);          
      douta : out std_logic_vector(15 downto 0)
      );
   end component;

   component blk_mem_gen_v7_4    -- bandstop filter
   port(
      clka : in std_logic;
      addra : in std_logic_vector(STAGES downto 0);          
      douta : out std_logic_vector(15 downto 0)
      );
   end component;

   -- constants
   
   -- signals
   signal l_bus, r_bus, l_bus_out, r_bus_out : std_logic_vector(17 downto 0); 
   signal cmd_addr : std_logic_vector(7 downto 0);
   signal cmd_data : std_logic_vector(15 downto 0);
   signal ready : std_logic;
   signal latching_cmd : std_logic;
   signal sig_source : std_logic_vector(2 downto 0) := "100";
   
   signal sig_rst : std_logic := '0';
   signal sig_x_l : std_logic_vector(15 downto 0) := (others => '0');
   signal sig_x_r : std_logic_vector(15 downto 0) := (others => '0');
   signal sig_y_l : std_logic_vector(15 downto 0) := (others => '0');
   signal sig_y_r : std_logic_vector(15 downto 0) := (others => '0');
   
   
   -- nuevo rom
   signal sig_addra : std_logic_vector(STAGES downto 0) := (others => '0');
   signal sig_douta : std_logic_vector(15 downto 0) := (others => '0'); 
   signal sig_douta_1 : std_logic_vector(15 downto 0) := (others => '0');  
   signal sig_douta_2 : std_logic_vector(15 downto 0) := (others => '0');  
   signal sig_douta_3 : std_logic_vector(15 downto 0) := (others => '0');  
   signal sig_douta_4 : std_logic_vector(15 downto 0) := (others => '0');  

   
begin
      
   -- instantiate both the main driver and ac97 chip configuration state-machine 
   -------------------------------------------------------------------------------
   ac97_cont0 : entity work.ac97(arch)
      port map(n_reset => n_reset,
               clk => clk,
               ac97_sdata_out => sdata_out, 
               ac97_sdata_in => sdata_in, 
               latching_cmd => latching_cmd ,
               ac97_sync => sync,
               ac97_bitclk => bit_clk,
               ac97_n_reset => ac97_n_reset,
               ac97_ready_sig => ready,
               l_out => l_bus,
               r_out => r_bus,
               l_in => l_bus_out,
               r_in => r_bus_out,
               cmd_addr => cmd_addr,
               cmd_data => cmd_data
               );
 
   ac97cmd_cont0 : entity work.ac97cmd(arch)
      port map (clk => clk, 
                ac97_ready_sig => ready,
                cmd_addr => cmd_addr,
                cmd_data => cmd_data,
                volume => volume, 
                source => sig_source, 
                latching_cmd => latching_cmd
                );  
                
   inst_fir_l: fir port map(
      clk => clk,
      rst => sig_rst,
      data_in => ready,
      data_out => open,
      filter_time => filter_time,
      x => sig_x_l,
      y => sig_y_l,
      
     addra => sig_addra,
     douta => sig_douta

   );

   inst_fir_r: fir port map(
      clk => clk,
      rst => sig_rst,
      data_in => ready,
      data_out => open,
      filter_time => open,
      x => sig_x_r,
      y => sig_y_r,
      
     addra => open,
     douta => sig_douta

   );
   
   
   
   -- nuevo rom
   inst_blk_mem_coef_lpf: blk_mem_gen_v7_1
   port map(
      clka => clk,
      addra => sig_addra,
      douta => sig_douta_1
   );
   --
   inst_blk_mem_coef_bpf: blk_mem_gen_v7_2
   port map(
      clka => clk,
      addra => sig_addra,
      douta => sig_douta_2
   );
   -- 
   inst_blk_mem_coef_bsf: blk_mem_gen_v7_3
   port map(
      clka => clk,
      addra => sig_addra,
      douta => sig_douta_3
   );
   -- 
   inst_blk_mem_coef_mbf: blk_mem_gen_v7_4
   port map(
      clka => clk,
      addra => sig_addra,
      douta => sig_douta_4
   );


   sig_douta <= sig_douta_1 when filtro = "00" else
                sig_douta_2 when filtro = "01" else
                sig_douta_3 when filtro = "10" else
                sig_douta_4;
                  
   
   led <= mode&filtro&volume;
   
   sig_rst <= not n_reset;
    
   --  latch output back into input for talkthrough testing
   --  this process can be replaced with a user component for signal processing 
   -------------------------------------------------------------------------------

   process (clk, n_reset, l_bus_out, r_bus_out)
   begin 
      if (clk'event and clk = '1') then
         if n_reset = '0' then
            sig_x_l <= (others => '0');
            sig_x_r <= (others => '0');
            l_bus <= (others => '0');
            r_bus <= (others => '0');
         elsif(ready = '1') then
            if mode = '1' then
--             if ( signed(l_bus_out(17 downto 2)) <= to_signed(32767,16) ) then
--                sig_x_l <= std_logic_vector(unsigned(l_bus_out(17 downto 2)) + 32767);
--             else
--                sig_x_l <= std_logic_vector(unsigned(l_bus_out(17 downto 2)) - 32767);
--             end if;
               sig_x_l <= l_bus_out(17 downto 2);
               sig_x_r <= r_bus_out(17 downto 2);
--             if ( signed(sig_y_l) <= to_signed(32767,16) ) then
--                l_bus(17 downto 2) <= std_logic_vector(unsigned(sig_y_l) + 32767);   
--             else
--                l_bus(17 downto 2) <= std_logic_vector(unsigned(sig_y_l) - 32767);
--             end if;
               l_bus(17 downto 2) <= sig_y_l;
               r_bus(17 downto 2) <= sig_y_r;
            else
               sig_x_l <= (others => '0');
               sig_x_r <= (others => '0');
               l_bus <= l_bus_out;
               r_bus <= r_bus_out;
            end if;
         end if;
      end if;
   end process;

end architecture;



