----------------------------------------------------------------------------------
-- Archivo:       binary_counter.vhd
-- Descripcion:   Contador binario de modulo M.
-- Entradas:         clk,rst  clock y reset
-- Salidas:          q        valor de la cuenta
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         16-11-2015
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.math_real.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity binary_counter is
   generic ( M : integer := 4);
   port (   clk   : in  std_logic;
            rst   : in  std_logic;
            q     : out  std_logic_vector (integer(ceil(log2(real (M))))-1 downto 0)
         );
end binary_counter;

architecture behavioral of binary_counter is

   -- constants
   constant N : integer := integer(ceil(log2(real (M)))); 

   -- signals
   signal sigcount : std_logic_vector (N-1 downto 0) := (others => '0');

begin

   process(clk)
   begin
      if rising_edge(clk) then
         if rst = '1' then
            sigcount <= (others => '0');
         elsif to_integer(unsigned(sigcount)) = M then
            sigcount <= (others => '0');
         else
            sigcount <= std_logic_vector(unsigned(sigcount) + 1);
         end if;
      end if;
   end process;

   q <= sigcount;

end behavioral;



