----------------------------------------------------------------------------------
-- Archivo:       counter.vhd
-- Descripcion:   Contador de modulo M.
-- Entradas:         clk,rst        clock y reset
-- Salidas:          tc, half_tc    terminal count
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         14-11-2015
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity counter is
	generic ( M : integer := 4 );
   port (   clk      : in  std_logic;
            rst      : in  std_logic;
            tc       : out  std_logic;
            half_tc  : out  std_logic
         );
end counter;

architecture behavioral of counter is

   -- constants
   constant n : integer := integer(ceil(log2(real (M))));

   -- signals
   signal count_a : std_logic_vector (n-1 downto 0) := (others => '0');
   signal count_f : std_logic_vector (n-1 downto 0) := (others => '0');

begin

   -- memory
   process(clk)
   begin
   	if rising_edge(clk) then
   		if rst = '1' then
   			count_a <= (others => '0');
   		else
   			count_a <= count_f;
   		end if;
   	end if;
   end process;

   -- next state logic
   process(rst,count_a)
   begin
   	if rst = '1' or to_integer(unsigned(count_a)) = (M-1) then
   		count_f <= (others => '0');
   	else
   		count_f <= std_logic_vector(unsigned(count_a) + 1);
   	end if;
   end process;

   -- output logic
   tc <= '1' when to_integer(unsigned(count_a)) = (M-1) else
   		'0';
   half_tc <= '1' when to_integer(unsigned(count_a)) = ((M-1)/2) else
   				'0';

end behavioral;



