----------------------------------------------------------------------------------
-- Archivo:       signed_adder.vhd
-- Descripcion:   Sumador de numeros signados en punto fijo 1.W-1 de longitud 
--                configurable con saturacion.
-- Generic:          W     longitud del registro
-- Entradas:         a     operando 1 (W bits)
--                   b     operando 2 (W bits)
-- Salidas:          r     salida (W bits)
--
-- Ejercicio:     TPI
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-10-2018
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity signed_adder is
   generic ( W    : natural := 16 );
   port  (  a     : in std_logic_vector(W-1 downto 0);
            b     : in std_logic_vector(W-1 downto 0);
            r     : out std_logic_vector(W-1 downto 0)
          );
end signed_adder;

architecture behavioral of signed_adder is

   -- constants
   constant zeros : std_logic_vector(W-2 downto 0) := (others => '0'); 
   constant ones : std_logic_vector(W-2 downto 0) := (others => '1');

   -- signals
   signal sig_a : signed(W-1 downto 0) := (others => '0');
   signal sig_b : signed(W-1 downto 0) := (others => '0');
   signal sig_r : signed(W-1 downto 0) := (others => '0');

   -- overflow
   signal sign_op : std_logic_vector(2 downto 0) := (others => '0');

begin

   -- casteo a signed
   sig_a <= signed(a);
   sig_b <= signed(b);

   -- resultado en 1.W-1
   sig_r <= sig_a + sig_b;

   -- evaluo overflow para saturar el resultado
   sign_op <= sig_a(W-1) & sig_b(W-1) & sig_r(W-1);

   r <=     '0' & ones     when  sign_op = "001"
      else  '1' & zeros    when  sign_op = "110"
      else  std_logic_vector(sig_r(W-1 downto 0));

end behavioral;



