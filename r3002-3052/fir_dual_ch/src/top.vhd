----------------------------------------------------------------------------------
-- archivo:       top.vhd
-- descripcion:   top level del proyecto fir_dual_ch Atlys Spartan 6.
-- board:         Atlys (Digilent)
-- fpga:          Spartan6 XC6SLX45 CSG324C (Xilinx)
-- entradas:         clk,rst_l               clock y reset
--                   sdin,bclk               codec
--                   volume,mode             control
-- salidas:          sdout,sync,ac97_rst_l   codec
--                   led                     status
--
-- autor:         agustin diaz antuna
-- contacto:      agustin.diazantuna@gmail.com
-- fecha:         24-06-2016
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity top is
   port (   clk         : in  std_logic;
            rst_l       : in  std_logic;
            -- codec ports
            sdin        : in std_logic;
            sdout       : out std_logic;
            sync        : out std_logic;
            bclk        : in std_logic;
            ac97_rst_l  : out std_logic;
            -- control ports
            volume      : in std_logic_vector(4 downto 0);
            mode        : in std_logic;
            -- led status
            led         : out std_logic_vector(7 downto 0)
         );
end top;


architecture behavioral of top is

   -------------------------------------------------------------------------------
   -- component
   -------------------------------------------------------------------------------
   component fir_dual_ch is
      generic ( W    : natural := 32 );
      port  (  x     : in std_logic_vector(W-1 downto 0);
               clk   : in std_logic;
               rst   : in std_logic;
               en    : in std_logic;
               y     : out std_logic_vector(W-1 downto 0)
             );
   end component;

   -- constants
   constant W : natural := 32;

   -- signals
   -- codec audio: ac97
   signal latching_cmd : std_logic;
   signal ready : std_logic;
   signal sig_din_chL, sig_din_chR : std_logic_vector(17 downto 0);
   signal sig_dout_chL_reg, sig_dout_chR_reg : std_logic_vector(17 downto 0);
   signal sig_dout_chL_next, sig_dout_chR_next : std_logic_vector(17 downto 0);

   signal cmd_addr : std_logic_vector(7 downto 0);
   signal cmd_data : std_logic_vector(15 downto 0);

   -- codec audio: ac97cmd
   signal sig_source : std_logic_vector(2 downto 0) := "100";
   
   -- rst_h
   signal sig_rst : std_logic;

	-- fir_dual_ch
	signal sig_x : std_logic_vector(W-1 downto 0);
   signal sig_y : std_logic_vector(W-1 downto 0);
   signal sig_en : std_logic;
   
begin

   -- instantiation     
   ac97_inst : entity work.ac97(arch)
      port map(n_reset => rst_l,
               clk => clk,
               ac97_sdata_out => sdout, 
               ac97_sdata_in => sdin, 
               latching_cmd => latching_cmd ,
               ac97_sync => sync,
               ac97_bitclk => bclk,
               ac97_n_reset => ac97_rst_l,
               ac97_ready_sig => ready,
               l_out => sig_dout_chL_reg,
               r_out => sig_dout_chR_reg,
               l_in => sig_din_chL,
               r_in => sig_din_chR,
               cmd_addr => cmd_addr,
               cmd_data => cmd_data
               );
 
   ac97cmd_inst : entity work.ac97cmd(arch)
      port map (clk => clk, 
                ac97_ready_sig => ready,
                cmd_addr => cmd_addr,
                cmd_data => cmd_data,
                volume => volume, 
                source => sig_source, 
                latching_cmd => latching_cmd
                ); 


   -------------------------------------------------------------------------------
   i_fir_dual_ch : fir_dual_ch
      generic map ( W => 32 )
      port map (  x => sig_x,
                  clk => clk,
                  rst => sig_rst,
                  en => sig_en,
                  y => sig_y
               );

   sig_x <= sig_din_chL(17 downto 2) & sig_din_chR(17 downto 2);
   -------------------------------------------------------------------------------
   

   sig_rst <= not rst_l;
   led(4 downto 0) <= volume;
--   led(6 downto 5) <= (others => '0');
	led(7) <= '0';
	led(5) <= '0';
	
   memory : process (clk, rst_l)
   begin 
      if rising_edge(clk) then
         if rst_l = '0' then
            sig_dout_chL_reg <= (others => '0');
            sig_dout_chR_reg <= (others => '0');
         else
            sig_dout_chL_reg <= sig_dout_chL_next;
            sig_dout_chR_reg <= sig_dout_chR_next;
         end if;
      end if;
   end process;


	led(6) <=   '1'   when  mode = '1'
      else     '0';

   sig_en <=   '1'  when (ready = '1')
      else     '0';
	
   sig_dout_chL_next <= sig_y(W-1 downto W/2) & "00"     when  (mode = '1')
      else              sig_din_chL;

   sig_dout_chR_next <= sig_y(W/2-1 downto 0) & "00"     when  (mode = '1')
      else              sig_din_chR;



end architecture;



