----------------------------------------------------------------------------------
-- Archivo:       fir_transpose.vhd
-- Descripcion:   Filtro FIR implementando topologia transpuesta
-- Generic:          W     longitud de las muestras
--                   L     cantidad de etapas del filtro
-- Entradas:         x     entrada (W bits)
--                   clk   clock
--                   rst   reset with '1'
--                   en    enable with '1'
-- Salidas:          y     salida (W bits)
--
-- Ejercicio:     TPI
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-10-2018
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity fir_transpose is
   generic ( W    : natural := 16;
             L    : natural := 32 );
   port  (  x     : in std_logic_vector(W-1 downto 0);
            clk   : in std_logic;
            rst   : in std_logic;
            en    : in std_logic;
            y     : out std_logic_vector(W-1 downto 0)
          );
end fir_transpose;

architecture behavioral of fir_transpose is

   -- components
   component signed_multiplier is
      generic ( W    : natural := W );
      port  (  a     : in std_logic_vector(W-1 downto 0);
               b     : in std_logic_vector(W-1 downto 0);
               p     : out std_logic_vector(W-1 downto 0)
            );
   end component;

   component signed_adder is
      generic ( W    : natural := W );
      port  (  a     : in std_logic_vector(W-1 downto 0);
               b     : in std_logic_vector(W-1 downto 0);
               r     : out std_logic_vector(W-1 downto 0)
             );
   end component;

   component ffd_en is
   generic ( W    : natural := W );
   port  (  d     : in std_logic_vector(W-1 downto 0);
            clk   : in std_logic;
            rst   : in std_logic;
            en    : in std_logic;
            q     : out std_logic_vector(W-1 downto 0)
          );
   end component;

   -- types
   type std_logic_array is array(L-1 downto 0) of std_logic_vector(W-1 downto 0);

   -- constants
  constant coeff : std_logic_array := ( "0000000001100100",
                                        "0000000000000110",
                                        "1111111101001000",
                                        "1111111101011001",
                                        "0000000011001000",
                                        "0000000110011011",
                                        "1111111110110010",
                                        "1111110100011011",
                                        "1111111010010000",
                                        "0000001110110111",
                                        "0000010011001011",
                                        "1111110011111010",
                                        "1111010101011001",
                                        "1111110111110000",
                                        "0001100010110111",
                                        "0011000110001000",
                                        "0011000110001000",
                                        "0001100010110111",
                                        "1111110111110000",
                                        "1111010101011001",
                                        "1111110011111010",
                                        "0000010011001011",
                                        "0000001110110111",
                                        "1111111010010000",
                                        "1111110100011011",
                                        "1111111110110010",
                                        "0000000110011011",
                                        "0000000011001000",
                                        "1111111101011001",
                                        "1111111101001000",
                                        "0000000000000110",
                                        "0000000001100100");

   -- signals
  signal q_reg : std_logic_array;
  signal q_next : std_logic_array;
  signal sig_mult : std_logic_array;
  signal sig_add : std_logic_array;
  signal sig_y : std_logic_vector (W-1 downto 0);


begin


   -- cell instances
   cell_instances: for i in 0 to L-1 generate
      
      -- lsb cell
      lsb_cell : if (i = 0) generate

         lsb_signed_multiplier : signed_multiplier
            generic map ( W => W )
            port map (  a => x,
                        b => coeff(0),
                        p => sig_add(0)
                     );

      end generate lsb_cell;


      -- cells from 1 to L-1
      not_mid_cell : if (i > 0) and (i < L) generate
         
         i_signed_multiplier : signed_multiplier
            generic map ( W => W )
            port map (  a => x,
                        b => coeff(i),
                        p => sig_mult(i)
                     );

         i_signed_adder : signed_adder
            generic map ( W => W )
            port map (  a => q_reg(i),
                        b => sig_mult(i),
                        r => sig_add(i)
                     );         

         i_ffd_en : ffd_en
            generic map ( W => W )
            port map (  d => q_next(i),
                        clk => clk,
                        rst => rst,
                        en => en,
                        q => q_reg(i)
                     );  

         q_next(i) <= sig_add(i-1);

      end generate not_mid_cell; 


      -- msb cell
      msb_cell : if (i = L-1) generate

         lsb_ffd_en : ffd_en
            generic map ( W => W )
            port map (  d => sig_add(L-1),
                        clk => clk,
                        rst => rst,
                        en => en,
                        q => sig_y
                     );  

      end generate msb_cell; 


   end generate;


   -- output
   y <= sig_y;


end behavioral;



