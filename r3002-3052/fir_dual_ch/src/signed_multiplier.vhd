----------------------------------------------------------------------------------
-- Archivo:       signed_multiplier.vhd
-- Descripcion:   Multiplicador de numeros signados en punto fijo 1.W-1 de 
--                longitud configurable.
-- Generic:          W     longitud de los operandos
-- Entradas:         a     operando 1 (W bits)
--                   b     operando 2 (W bits)
-- Salidas:          p     salida (W bits)
--
-- Ejercicio:     TPI
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-10-2018
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity signed_multiplier is
   generic ( W    : natural := 16 );
   port  (  a     : in std_logic_vector(W-1 downto 0);
            b     : in std_logic_vector(W-1 downto 0);
            p     : out std_logic_vector(W-1 downto 0)
          );
end signed_multiplier;

architecture behavioral of signed_multiplier is

   -- constants

   -- signals
   signal sig_a : signed(W-1 downto 0) := (others => '0');
   signal sig_b : signed(W-1 downto 0) := (others => '0');
   signal sig_p : signed((2*W)-1 downto 0) := (others => '0');

begin

   -- casteo a signed
   sig_a <= signed(a);
   sig_b <= signed(b);

   -- resultado en 2.(2*W - 2)
   sig_p <= sig_a * sig_b;

   p <= std_logic_vector(sig_p((2*W)-2 downto W-1));

end behavioral;



