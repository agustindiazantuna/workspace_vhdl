----------------------------------------------------------------------------------
-- Archivo:       fir_dual_ch.vhd
-- Descripcion:   Filtro FIR transpuesto de dos canales
-- Generic:          W     longitud de las muestras
--                   L     cantidad de etapas del filtro
-- Entradas:         x     entrada (W bits)
--                   clk   clock
--                   rst   reset with '1'
--                   en    enable with '1'
-- Salidas:          y     salida (W bits)
--
-- Ejercicio:     TPI
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         30-10-2018
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity fir_dual_ch is
   generic ( W    : natural := 32 );
   port  (  x     : in std_logic_vector(W-1 downto 0);
            clk   : in std_logic;
            rst   : in std_logic;
            en    : in std_logic;
            y     : out std_logic_vector(W-1 downto 0)
          );
end fir_dual_ch;

architecture behavioral of fir_dual_ch is

   -- components
  component fir_transpose is
     generic ( W    : natural := W/2;
               L    : natural := 32 );
     port  (  x     : in std_logic_vector(W-1 downto 0);
              clk   : in std_logic;
              rst   : in std_logic;
              en    : in std_logic;
              y     : out std_logic_vector(W-1 downto 0)
            );
  end component;

   -- types

   -- constants

   -- signals
  signal sig_x : std_logic_vector (W-1 downto 0);


begin


   -- fir instances
   left_fir_transpose : fir_transpose
      generic map ( W => W/2 )
      port map (  x => x(W-1 downto W/2),
                  clk => clk,
                  rst => rst,
                  en => en,
                  y => y(W-1 downto W/2)
               );

   right_fir_transpose : fir_transpose
      generic map ( W => W/2 )
      port map (  x => x(W/2 - 1 downto 0),
                  clk => clk,
                  rst => rst,
                  en => en,
                  y => y(W/2 - 1 downto 0)
               );


end behavioral;



