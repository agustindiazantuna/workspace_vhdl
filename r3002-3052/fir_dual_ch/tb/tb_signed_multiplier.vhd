library IEEE;
use	ieee.std_logic_1164.all;
use	ieee.numeric_std.all;
use ieee.std_logic_textio.all;

entity tb_signed_multiplier is
  generic(
    W: natural := 4
  );
end tb_signed_multiplier;

architecture simulation of tb_signed_multiplier is
	signal tb_a : std_logic_vector (W-1 downto 0) := (others => '0');
	signal tb_b : std_logic_vector (W-1 downto 0) := (others => '0');
	signal tb_p : std_logic_vector (W-1 downto 0);
  	
  constant STEP_TIME : time := 10 ns;
	constant SYN_DELAY : time :=  1 ns;
	
begin

	uut: entity work.signed_multiplier
	  generic map( W => W )
		port map(
		  a => tb_a,
		  b => tb_b,
		  p => tb_p
    );
    
	stim_process : process
	  variable s_a    : signed   (  W-1 downto 0) := (others => '0');
	  variable s_b    : signed   (  W-1 downto 0) := (others => '0');
		variable gldn   : signed   (2*W-1 downto 0) := (others => '0');
		variable errors : integer                   := 0;
	begin
	
    assert false 
      report LF & 
      "***************************************" & LF & 
      "           SIMULATION BEGINS           " & LF & 
      "***************************************" & LF & LF
      severity note;	
	
		for i in -2**(W-1) to 2**(W-1)-1 loop
			tb_a <= std_logic_vector(to_signed(i,W));
			for j in -2**(W-1) to 2**(W-1)-1 loop
			  tb_b <= std_logic_vector(to_signed(j,W));
  			gldn := to_signed(i,W) * to_signed(j,W);
  			wait for STEP_TIME;
				if (tb_p /= std_logic_vector(gldn(2*W-2 downto W-1))) then 
				  errors := errors + 1;
				  report "!!! Assert violation @ i=" & INTEGER'IMAGE(i) & " and j=" & INTEGER'IMAGE(j) & "!!!" & LF &
				         " Expected value = " & INTEGER'IMAGE(to_integer(gldn)) & LF &
				         " Current  value = " & INTEGER'IMAGE(to_integer(signed(tb_p))) & LF
					severity warning;
				end if;
      end loop;
		end loop;
	
    if (errors = 0) then
      report LF & 
      "***************************************" & LF & 
      "   NO ERRORS FOUND    SUCCESSFUL SIM   " & LF &
      "***************************************" & LF & LF;
    else
      report LF & 
      "***************************************" & LF & 
      "   FAILED TEST          ERRORS FOUND   " & LF & 
      "***************************************" & LF & LF;
    end if;


	  assert false 
      report LF & 
      "***************************************" & LF &
      "           SIMULATION   ENDS           " & LF &
      "***************************************" & LF & LF
      severity failure;
      
	end process;

end simulation;
