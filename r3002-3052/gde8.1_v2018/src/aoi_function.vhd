----------------------------------------------------------------------------------
-- Archivo:       aoi_function.vhd
-- Descripcion:   Circuito que cumple la funcion logica AOI (and-or-inversor).
-- Entradas:         a, b, c, d
-- Salidas:          o
--
-- Ejercicio:     8.1
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity aoi_function is
   port  (  a  : in std_logic;
            b  : in std_logic;
            c  : in std_logic;
            d  : in std_logic;
            o  : out std_logic
          );
end aoi_function;

architecture behavioral of aoi_function is

   -- constants

   -- signals

begin

   o <= not( (a and b) or (c and d) );

end behavioral;



