----------------------------------------------------------------------------------
-- Archivo:       logic_unit.vhd
-- Descripcion:   Circuito encargado de resolver operaciones logicas.
-- Entradas:         a,b   datos
--                   op    codigo de operacion
-- Salidas:          r     resultado
--
-- Ejercicio:     8.10
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------

----------------------
-- | op | resultado  |
-- | 00 |   a and b  |
-- | 01 |   a or b   |
-- | 10 |   a xor b  |
-- | 11 |   a xnor b |
----------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity logic_unit is
   port  (  a  : in std_logic_vector(7 downto 0);
            b  : in std_logic_vector(7 downto 0);
            op : in std_logic_vector(1 downto 0);
            r  : out std_logic_vector(7 downto 0)
          );
end logic_unit;

architecture behavioral of logic_unit is

   -- constants

   -- signals


begin

   r <=     (a and b) when op = "00"
      else  (a or b) when op = "01"
      else  (a xor b) when op = "10"
      else  (a xnor b);

end behavioral;



