----------------------------------------------------------------------------------
-- Archivo:       oai_function.vhd
-- Descripcion:   Circuito que cumple la funcion logica OAI (or-and-inversor).
-- Entradas:         a, b, c, d
-- Salidas:          o
--
-- Ejercicio:     8.1
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity oai_function is
   port  (  a  : in std_logic;
            b  : in std_logic;
            c  : in std_logic;
            d  : in std_logic;
            o  : out std_logic
          );
end oai_function;

architecture behavioral of oai_function is

   -- constants

   -- signals

begin

   o <= not( (a or b) and (c or d) );

end behavioral;



