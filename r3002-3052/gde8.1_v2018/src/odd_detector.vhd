----------------------------------------------------------------------------------
-- Archivo:       odd_detector.vhd
-- Descripcion:   Circuito detector de paridad: IMPAR.
-- Entradas:         a     datos
-- Salidas:          odd   odd <= '0' si la cantidad de unos es impar
--
-- Ejercicio:     8.4
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity odd_detector is
   port  (  a     : in std_logic_vector(2 downto 0);
            odd   : out std_logic
          );
end odd_detector;

architecture behavioral of odd_detector is

   -- constants

   -- signals

begin

   odd <= (a(2) xor a(1)) xor a(0);

end behavioral;



