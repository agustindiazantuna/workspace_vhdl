----------------------------------------------------------------------------------
-- Archivo:       encoder_8to3.vhd
-- Descripcion:   Circuito codificador 8 a 3.
-- Entradas:         i  datos
-- Salidas:          o  salida codificada
--                   g  senial de grupo
--
-- Ejercicio:     8.9
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity encoder_8to3 is
   port  (  i  : in std_logic_vector(7 downto 0);
            o  : out std_logic_vector(2 downto 0);
            g  : out std_logic
          );
end encoder_8to3;

architecture behavioral of encoder_8to3 is

   -- constants

   -- signals
   signal sig_o : std_logic_vector(7 downto 0) := (others => '0');

begin

   -- salida codificada
   o <=     "111" when i(7) = '1'
      else  "110" when i(6) = '1'
      else  "101" when i(5) = '1'
      else  "100" when i(4) = '1'
      else  "011" when i(3) = '1'
      else  "010" when i(2) = '1'
      else  "001" when i(1) = '1'
      else  (others => '0');


   -- group signal
   g <=     '0' when i = x"00"
      else  '1';

end behavioral;



