----------------------------------------------------------------------------------
-- Archivo:       mux_2to1.vhd
-- Descripcion:   Circuito multiplexor 2 a 1.
-- Entradas:         d0,d1    datos
--                   sel      entrada de seleccion
-- Salidas:          output   salida comandada por sel
--
-- Ejercicio:     8.5
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity mux_2to1 is
   port  (  d0       : in std_logic_vector(7 downto 0);
            d1       : in std_logic_vector(7 downto 0);
            sel      : in std_logic;
            output   : out std_logic_vector(7 downto 0)
          );
end mux_2to1;

architecture behavioral of mux_2to1 is

   -- constants

   -- signals

begin

   with sel select
      output <=   d0 when '0',
                  d1 when others;

end behavioral;



