----------------------------------------------------------------------------------
-- Archivo:       even_detector.vhd
-- Descripcion:   Circuito detector de paridad: PAR.
-- Entradas:         a     datos
-- Salidas:          even  even <= '1' si la cantidad de unos es par
--
-- Ejercicio:     8.3
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity even_detector is
   port  (  a     : in std_logic_vector(2 downto 0);
            even  : out std_logic
          );
end even_detector;

architecture behavioral of even_detector is

   -- constants

   -- signals

begin

   even <= not( (a(2) xor a(1)) xor a(0) );

end behavioral;



