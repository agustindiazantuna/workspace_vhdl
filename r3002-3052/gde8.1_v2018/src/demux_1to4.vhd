----------------------------------------------------------------------------------
-- Archivo:       demux_1to4.vhd
-- Descripcion:   Circuito demultiplexor 1 a 4.
-- Entradas:         din               datos
--                   sel               entrada de seleccion
-- Salidas:          do0,do1,do2,do3   salidas comandada por sel
--
-- Ejercicio:     8.6
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity demux_1to4 is
   port  (  din   : in std_logic_vector(7 downto 0);
            sel   : in std_logic_vector(1 downto 0);
            do0   : out std_logic_vector(7 downto 0);
            do1   : out std_logic_vector(7 downto 0);
            do2   : out std_logic_vector(7 downto 0);
            do3   : out std_logic_vector(7 downto 0)
          );
end demux_1to4;

architecture behavioral of demux_1to4 is

   -- constants

   -- signals

begin

   do0 <=   din when sel = "00"
      else  (others => '0');

   do1 <=   din when sel = "01"
      else  (others => '0');

   do2 <=   din when sel = "10"
      else  (others => '0');

   do3 <=   din when sel = "11"
      else  (others => '0');

   
end behavioral;



