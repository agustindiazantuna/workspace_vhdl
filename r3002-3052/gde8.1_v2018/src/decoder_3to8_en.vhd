----------------------------------------------------------------------------------
-- Archivo:       decoder_3to8_en.vhd
-- Descripcion:   Circuito decodificador 3 a 8 con enable.
-- Entradas:         a  datos
--                   en entrada de habilitacion
-- Salidas:          o  salidas decodificada
--
-- Ejercicio:     8.8
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         17-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity decoder_3to8_en is
   port  (  a  : in std_logic_vector(2 downto 0);
            en : in std_logic;
            o  : out std_logic_vector(7 downto 0)
          );
end decoder_3to8_en;

architecture behavioral of decoder_3to8_en is

   -- constants

   -- signals
   signal sig_o : std_logic_vector(7 downto 0) := (others => '0');

begin

   with a select
      sig_o <= x"01" when "000",
               x"02" when "001",
               x"04" when "010",
               x"08" when "011",
               x"10" when "100",
               x"20" when "101",
               x"40" when "110",
               x"80" when others;

   o <=     sig_o when en = '1'
      else  (others => '0');

end behavioral;



