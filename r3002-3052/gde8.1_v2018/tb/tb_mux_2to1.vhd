----------------------------------------------------------------------------------
-- Archivo:       tb_mux_2to1.vhd
-- Descripcion:   Testbench de: Circuito multiplexor 2 a 1.
--
-- Ejercicio:     8.5
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_mux_2to1 is
end tb_mux_2to1;
 
architecture behavior of tb_mux_2to1 is 
 
    -- component declaration for the unit under test (uut)
    component mux_2to1
      port( d0       : in std_logic_vector(7 downto 0);
            d1       : in std_logic_vector(7 downto 0);
            sel      : in std_logic;
            output   : out std_logic_vector(7 downto 0)
          );
    end component;


   --inputs
   signal d0 : std_logic_vector(7 downto 0) := (others => '0');
   signal d1 : std_logic_vector(7 downto 0) := (others => '0');
   signal sel : std_logic := '0';

   --outputs
   signal output : std_logic_vector(7 downto 0) := (others => '0');


begin
 
   -- instantiate the unit under test (uut)
    uut: mux_2to1 port map (
          d0 => d0,
          d1 => d1,
          sel => sel,
          output => output
        );

   -- stimulus process
   stim_proc: process
   begin    
      d0 <= x"FA";
      d1 <= x"05";
      sel <= '0';
      wait for 20 ns;
      sel <= '1';
      wait for 20 ns;
      d0 <= x"CC";
      d1 <= x"33";
      wait for 20 ns;
      sel <= '0';
      wait for 20 ns;
    
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



