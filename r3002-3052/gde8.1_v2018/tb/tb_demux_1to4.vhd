----------------------------------------------------------------------------------
-- Archivo:       tb_demux_1to4.vhd
-- Descripcion:   Testbench de: Circuito demultiplexor 1 a 4.
--
-- Ejercicio:     8.6
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_demux_1to4 is
end tb_demux_1to4;
 
architecture behavior of tb_demux_1to4 is
 
    -- component declaration for the unit under test (uut)
    component demux_1to4
    port(
         din : in  std_logic_vector(7 downto 0);
         sel : in  std_logic_vector(1 downto 0);
         do0 : out  std_logic_vector(7 downto 0);
         do1 : out  std_logic_vector(7 downto 0);
         do2 : out  std_logic_vector(7 downto 0);
         do3 : out  std_logic_vector(7 downto 0)
        );
    end component;
    

   --inputs
   signal din : std_logic_vector(7 downto 0) := (others => '0');
   signal sel : std_logic_vector(1 downto 0) := (others => '0');

 	--outputs
   signal do0 : std_logic_vector(7 downto 0);
   signal do1 : std_logic_vector(7 downto 0);
   signal do2 : std_logic_vector(7 downto 0);
   signal do3 : std_logic_vector(7 downto 0);


begin
 
   -- instantiate the unit under test (uut)
   uut: demux_1to4 port map (
          din => din,
          sel => sel,
          do0 => do0,
          do1 => do1,
          do2 => do2,
          do3 => do3
        );

   -- stimulus process
   stim_proc: process
   begin    
      din <= x"FA";
      sel <= "00";
      wait for 20 ns;
      din <= x"05";
      sel <= "01";
      wait for 20 ns;
      din <= x"A5";
      sel <= "10";
      wait for 20 ns;
      din <= x"C3";
      sel <= "11";
      wait for 20 ns;
    
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



