----------------------------------------------------------------------------------
-- Archivo:       tb_oai_function.vhd
-- Descripcion:   Testbench de: Circuito que cumple la funcion logica OAI
--                (or-and-inversor).
--
-- Ejercicio:     8.2
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_oai_function is
end tb_oai_function;
 
architecture behavior of tb_oai_function is 
 
    -- component declaration for the unit under test (uut)
    component oai_function
      port( a  : in std_logic;
            b  : in std_logic;
            c  : in std_logic;
            d  : in std_logic;
            o  : out std_logic
          );
    end component;


   --inputs
   signal a : std_logic := '0';
   signal b : std_logic := '0';
   signal c : std_logic := '0';
   signal d : std_logic := '0';

   --outputs
   signal o : std_logic;


begin
 
   -- instantiate the unit under test (uut)
    uut: oai_function port map (
          a => a,
          b => b,
          c => c,
          d => d,
          o => o
        );

   -- stimulus process
   stim_proc: process
   begin    
      a <= '0';
      b <= '0';
      c <= '0';
      d <= '0';
      wait for 20 ns;
      a <= '0';
      b <= '0';
      c <= '0';
      d <= '1';
      wait for 20 ns;
      a <= '0';
      b <= '0';
      c <= '1';
      d <= '0';
      wait for 20 ns;
      a <= '0';
      b <= '0';
      c <= '1';
      d <= '1';
      wait for 20 ns;
      a <= '0';
      b <= '1';
      c <= '0';
      d <= '0';
      wait for 20 ns;
      a <= '0';
      b <= '1';
      c <= '0';
      d <= '1';
      wait for 20 ns;
      a <= '0';
      b <= '1';
      c <= '1';
      d <= '0';
      wait for 20 ns;
      a <= '0';
      b <= '1';
      c <= '1';
      d <= '1';
      wait for 20 ns;
      a <= '1';
      b <= '0';
      c <= '0';
      d <= '0';
      wait for 20 ns;
      a <= '1';
      b <= '0';
      c <= '0';
      d <= '1';
      wait for 20 ns;
      a <= '1';
      b <= '0';
      c <= '1';
      d <= '0';
      wait for 20 ns;
      a <= '1';
      b <= '0';
      c <= '1';
      d <= '1';
      wait for 20 ns;
      a <= '1';
      b <= '1';
      c <= '0';
      d <= '0';
      wait for 20 ns;
      a <= '1';
      b <= '1';
      c <= '0';
      d <= '1';
      wait for 20 ns;
      a <= '1';
      b <= '1';
      c <= '1';
      d <= '0';
      wait for 20 ns;
      a <= '1';
      b <= '1';
      c <= '1';
      d <= '1';
      wait for 20 ns;
    
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



