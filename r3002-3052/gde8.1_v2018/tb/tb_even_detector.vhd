----------------------------------------------------------------------------------
-- Archivo:       tb_even_detector.vhd
-- Descripcion:   Testbench de: Circuito detector de paridad: PAR.
--
-- Ejercicio:     8.3
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_even_detector is
end tb_even_detector;
 
architecture behavior of tb_even_detector is 
 
    -- component declaration for the unit under test (uut)
    component even_detector
      port( a     : in std_logic_vector(2 downto 0);
            even  : out std_logic
          );
    end component;


   --inputs
   signal a : std_logic_vector(2 downto 0) := (others => '0');

   --outputs
   signal even : std_logic := '0';


begin
 
   -- instantiate the unit under test (uut)
    uut: even_detector port map (
          a => a,
          even => even
        );

   -- stimulus process
   stim_proc: process
   begin    
      a <= "000";
      wait for 20 ns;
      a <= "001";
      wait for 20 ns;
      a <= "010";
      wait for 20 ns;
      a <= "011";
      wait for 20 ns;
      a <= "100";
      wait for 20 ns;
      a <= "101";
      wait for 20 ns;
      a <= "110";
      wait for 20 ns;
      a <= "111";
      wait for 20 ns;
    
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



