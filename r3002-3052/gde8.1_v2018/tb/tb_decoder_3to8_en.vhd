----------------------------------------------------------------------------------
-- Archivo:       tb_decoder_3to8_en.vhd
-- Descripcion:   Testbench de: Circuito decodificador 3 a 8 con enable.
--
-- Ejercicio:     8.8
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_decoder_3to8_en is
end tb_decoder_3to8_en;
 
architecture behavior of tb_decoder_3to8_en is
 
    -- component declaration for the unit under test (uut)
    component decoder_3to8_en
    port(
         a : in  std_logic_vector(2 downto 0);
         en : in  std_logic;
         o : out  std_logic_vector(7 downto 0)
        );
    end component;
    

   --inputs
   signal a : std_logic_vector(2 downto 0) := (others => '0');
   signal en : std_logic := '0';

  --outputs
   signal o : std_logic_vector(7 downto 0);

 
begin
 
  -- instantiate the unit under test (uut)
   uut: decoder_3to8_en port map (
          a => a,
          en => en,
          o => o
        );

   -- stimulus process
   stim_proc: process
   begin
      en <= '1';
      a <= "000";
      wait for 20 ns;
      a <= "001";
      wait for 20 ns;
      a <= "010";
      wait for 20 ns;
      a <= "011";
      wait for 20 ns;
      a <= "100";
      wait for 20 ns;
      a <= "101";
      wait for 20 ns;
      a <= "110";
      wait for 20 ns;
      a <= "111";
      wait for 20 ns;

      en <= '0';
      a <= "000";
      wait for 20 ns;
      a <= "001";
      wait for 20 ns;
      a <= "010";
      wait for 20 ns;
      a <= "011";
      wait for 20 ns;
      a <= "100";
      wait for 20 ns;
      a <= "101";
      wait for 20 ns;
      a <= "110";
      wait for 20 ns;
      a <= "111";
      wait for 20 ns;
      
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



