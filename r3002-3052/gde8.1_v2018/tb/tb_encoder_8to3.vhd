----------------------------------------------------------------------------------
-- Archivo:       tb_encoder_8to3.vhd
-- Descripcion:   Testbench de: Circuito codificador 8 a 3.
--
-- Ejercicio:     8.9
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_encoder_8to3 is
end tb_encoder_8to3;
 
architecture behavior of tb_encoder_8to3 is
 
    -- component declaration for the unit under test (uut)
    component encoder_8to3
    port(
         i : in  std_logic_vector(7 downto 0);
         o : out  std_logic_vector(2 downto 0);
         g : out  std_logic
        );
    end component;
    

   --inputs
   signal i : std_logic_vector(7 downto 0) := (others => '0');

   --outputs
   signal o : std_logic_vector(2 downto 0) := (others => '0');
   signal g : std_logic := '0';

 
begin
 
  -- instantiate the unit under test (uut)
   uut: encoder_8to3 port map (
          i => i,
          o => o,
          g => g
        );

   -- stimulus process
   stim_proc: process
   begin
      i <= x"00";
      wait for 20 ns;
      i <= x"01";
      wait for 20 ns;
      i <= x"03";
      wait for 20 ns;
      i <= x"07";
      wait for 20 ns;
      i <= x"0F";
      wait for 20 ns;
      i <= x"1F";
      wait for 20 ns;
      i <= x"3F";
      wait for 20 ns;
      i <= x"7F";
      wait for 20 ns;
      i <= x"FF";
      wait for 20 ns;
      
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



