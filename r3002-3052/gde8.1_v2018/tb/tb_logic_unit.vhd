----------------------------------------------------------------------------------
-- Archivo:       tb_logic_unit.vhd
-- Descripcion:   Testbench de: Circuito encargado de resolver operaciones 
--                logicas.
--
-- Ejercicio:     8.10
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_logic_unit is
end tb_logic_unit;
 
architecture behavior of tb_logic_unit is
 
    -- component declaration for the unit under test (uut)
    component logic_unit
    port(
         a : in  std_logic_vector(7 downto 0);
         b : in  std_logic_vector(7 downto 0);
         op : in  std_logic_vector(1 downto 0);
         r : out  std_logic_vector(7 downto 0)
        );
    end component;
    

   --inputs
   signal a : std_logic_vector(7 downto 0) := (others => '0');
   signal b : std_logic_vector(7 downto 0) := (others => '0');
   signal op : std_logic_vector(1 downto 0) := (others => '0');

   --outputs
   signal r : std_logic_vector(7 downto 0);

 
begin
 
   -- instantiate the unit under test (uut)
   uut: logic_unit port map (
          a => a,
          b => b,
          op => op,
          r => r
        );

   -- stimulus process
   stim_proc: process
   begin
      a <= x"5A";
      b <= x"A5";
      op <= "00";
      wait for 20 ns;
      a <= x"5A";
      b <= x"A5";
      op <= "01";
      wait for 20 ns;
      a <= x"5A";
      b <= x"A5";
      op <= "10";
      wait for 20 ns;
      a <= x"5A";
      b <= x"A5";
      op <= "11";
      wait for 20 ns;
      
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



