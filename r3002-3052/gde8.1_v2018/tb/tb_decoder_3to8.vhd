----------------------------------------------------------------------------------
-- Archivo:       tb_decoder_3to8.vhd
-- Descripcion:   Testbench de: Circuito decodificador 3 a 8.
--
-- Ejercicio:     8.7
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_decoder_3to8 is
end tb_decoder_3to8;
 
architecture behavior of tb_decoder_3to8 is
 
    -- component declaration for the unit under test (uut)
    component decoder_3to8
    port(
         a : in  std_logic_vector(2 downto 0);
         o : out  std_logic_vector(7 downto 0)
        );
    end component;
    

   --inputs
   signal a : std_logic_vector(2 downto 0) := (others => '0');

  --outputs
   signal o : std_logic_vector(7 downto 0);

 
begin
 
  -- instantiate the unit under test (uut)
   uut: decoder_3to8 port map (
          a => a,
          o => o
        );

   -- stimulus process
   stim_proc: process
   begin    
      a <= "000";
      wait for 20 ns;
      a <= "001";
      wait for 20 ns;
      a <= "010";
      wait for 20 ns;
      a <= "011";
      wait for 20 ns;
      a <= "100";
      wait for 20 ns;
      a <= "101";
      wait for 20 ns;
      a <= "110";
      wait for 20 ns;
      a <= "111";
      wait for 20 ns;

    report "Simulation completed";
    assert false severity failure;
   end process;

end;



