----------------------------------------------------------------------------------
-- Archivo:       barrel_shifter.vhd
-- Descripcion:   Circuito que realiza desplazamientos.
-- Entradas:         x     datos
--                   dir   direccion de desplazamiento
--                   amt   cantidad de bits a desplazar
-- Salidas:          y     resultado
--
-- Ejercicio:     8.14
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------

-------------------------------------------
-- si dir = ’0’ => desplaza a la izquierda
-- si dir = ’1’ => desplaza a la derecha
-- amt determina el # de desplazamientos
-------------------------------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity barrel_shifter is
   port  (  x     : in std_logic_vector(7 downto 0);
            dir   : in std_logic;
            amt   : in std_logic_vector(2 downto 0);
            y     : out std_logic_vector(7 downto 0)
          );
end barrel_shifter;

architecture behavioral of barrel_shifter is

   -- constants

   -- signals
   signal sig_ctrl : std_logic_vector(3 downto 0) := (others => '0');

begin

   sig_ctrl <= dir & amt;

   with sig_ctrl select
      y  <= x                       when "0000",
            x(6 downto 0)&'0'       when "0001",
            x(5 downto 0)&"00"      when "0010",
            x(4 downto 0)&"000"     when "0011",
            x(3 downto 0)&"0000"    when "0100",
            x(2 downto 0)&"00000"   when "0101",
            x(1 downto 0)&"000000"  when "0110",
            x(0 downto 0)&"0000000" when "0111",

            x                       when "1000",
            "0"&x(7 downto 1)       when "1001",
            "00"&x(7 downto 2)      when "1010",
            "000"&x(7 downto 3)     when "1011",
            "0000"&x(7 downto 4)    when "1100",
            "00000"&x(7 downto 5)   when "1101",
            "000000"&x(7 downto 6)  when "1110",
            "0000000"&x(7 downto 7) when others;

end behavioral;



