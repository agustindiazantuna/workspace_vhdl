----------------------------------------------------------------------------------
-- Archivo:       arith_unit.vhd
-- Descripcion:   Circuito encargado de resolver operaciones aritmeticas.
-- Entradas:         a,b   datos
--                   op    codigo de operacion
-- Salidas:          r     resultado
--
-- Ejercicio:     8.15
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------

----------------------
-- | op | resultado  |
-- | 00 |   a + 1    |
-- | 01 |   a - 1    |
-- | 10 |   a + b    |
-- | 11 |   a - b    |
----------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity arith_unit is
   port  (  a  : in std_logic_vector(7 downto 0);
            b  : in std_logic_vector(7 downto 0);
            op : in std_logic_vector(1 downto 0);
            r  : out std_logic_vector(7 downto 0)
          );
end arith_unit;

architecture behavioral of arith_unit is

   -- constants

   -- signals
   signal sig_a : unsigned(7 downto 0);
   signal sig_b : unsigned(7 downto 0);

begin

   sig_a <= unsigned(a);
   sig_b <= unsigned(b);

   r <=     std_logic_vector(sig_a + 1)      when op = "00"
      else  std_logic_vector(sig_a - 1)      when op = "01"
      else  std_logic_vector(sig_a + sig_b)  when op = "10"
      else  std_logic_vector(sig_a - sig_b);

end behavioral;



