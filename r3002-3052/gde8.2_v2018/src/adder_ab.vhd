----------------------------------------------------------------------------------
-- Archivo:       adder_ab.vhd
-- Descripcion:   Circuito sumador de dos entradas de datos.
-- Entradas:         a,b   datos
-- Salidas:          r     resultado = a + b
--
-- Ejercicio:     8.11
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity adder_ab is
   port  (  a  : in std_logic_vector(15 downto 0);
            b  : in std_logic_vector(15 downto 0);
            r  : out std_logic_vector(15 downto 0)
          );
end adder_ab;

architecture behavioral of adder_ab is

   -- constants

   -- signals
   signal sig_a : unsigned(15 downto 0);
   signal sig_b : unsigned(15 downto 0);

begin

   sig_a <= unsigned(a);
   sig_b <= unsigned(b);
   r <= std_logic_vector(sig_a + sig_b);

end behavioral;



