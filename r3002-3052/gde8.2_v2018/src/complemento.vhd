----------------------------------------------------------------------------------
-- Archivo:       complemento.vhd
-- Descripcion:   Circuito que realiza el complemento de un numero.
-- Entradas:         x     datos
--                   ctrl  entrada de control
-- Salidas:          r     CA1(x) si ctrl = 0
--                         CA2(x) si ctrl = 1
--
-- Ejercicio:     8.12
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity complemento is
   port  (  x     : in std_logic_vector(7 downto 0);
            ctrl  : in std_logic;
            y     : out std_logic_vector(7 downto 0)
          );
end complemento;

architecture behavioral of complemento is

   -- constants

   -- signals
	signal sig_x : unsigned(7 downto 0) := (others => '0');

begin

   sig_x <= not(unsigned(x));

   with ctrl select
      y  <= std_logic_vector(sig_x)       when '0',
            std_logic_vector(sig_x + 1)   when others;

end behavioral;



