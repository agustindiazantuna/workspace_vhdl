----------------------------------------------------------------------------------
-- Archivo:       arith_unit2.vhd
-- Descripcion:   Circuito encargado de resolver operaciones aritmeticas.
-- Entradas:         a,b         datos
--                   op          codigo de operacion
-- Salidas:          r           resultado
--                   co,ov,n,z   flags
--
-- Ejercicio:     8.15
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------

----------------------
-- | op | resultado  |
-- | 00 |   a + 1    |
-- | 01 |   a - 1    |
-- | 10 |   a + b    |
-- | 11 |   a - b    |
----------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity arith_unit2 is
   port  (  a  : in std_logic_vector(7 downto 0);
            b  : in std_logic_vector(7 downto 0);
            op : in std_logic_vector(1 downto 0);
            r  : out std_logic_vector(7 downto 0);
            co : out std_logic;
            ov : out std_logic;
            n  : out std_logic;
            z  : out std_logic
          );
end arith_unit2;

architecture behavioral of arith_unit2 is

   -- constants

   -- signals
   signal sig_a : signed(8 downto 0);
   signal sig_b : signed(8 downto 0);
   signal sig_r : std_logic_vector(8 downto 0);

   signal sig_ov_add : std_logic;
   signal sig_ov_sub : std_logic;
   signal sig_ov : std_logic_vector(2 downto 0);

begin

   sig_a <= a(7) & signed(a);
   sig_b <= b(7) & signed(b);
   sig_ov <= sig_r(8) & sig_a(8) & sig_b(8);

   -- resultado
   sig_r <= std_logic_vector(sig_a + 1)      when op = "00"
      else  std_logic_vector(sig_a - 1)      when op = "01"
      else  std_logic_vector(sig_a + sig_b)  when op = "10"
      else  std_logic_vector(sig_a - sig_b);

   r <= sig_r(7 downto 0);

   -- flags
   co <= sig_r(8);

   n <= sig_r(7);
   
   z <=     '0'   when  sig_r /= x"000"
      else  '1';

   -- overflow
   with sig_ov select
      sig_ov_add <=  '1' when "100"|"011",
                     '0' when others;

   with sig_ov select
      sig_ov_sub <=  '1' when "010"|"101",
                     '0' when others;

   ov <=    sig_ov_add when op(0) = '0'
      else  sig_ov_sub;


end behavioral;



