----------------------------------------------------------------------------------
-- Archivo:       adder_const.vhd
-- Descripcion:   Circuito sumador de un dato con una constante.
-- Entradas:         a,b   datos
-- Salidas:          r     resultado = a + CONSTANTE
--
-- Ejercicio:     8.11
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity adder_const is
   port  (  a  : in std_logic_vector(15 downto 0);
            r  : out std_logic_vector(15 downto 0)
          );
end adder_const;

architecture behavioral of adder_const is

   -- constants
   -- probar con: x"0001", x"0080", x"8000", x"AAAA"
   constant const_b : unsigned(15 downto 0) := x"0001";

   -- signals
   signal sig_a : unsigned(15 downto 0);

begin

   sig_a <= unsigned(a);
   r <= std_logic_vector(sig_a + const_b);

end behavioral;



