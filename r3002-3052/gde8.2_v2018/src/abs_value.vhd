----------------------------------------------------------------------------------
-- Archivo:       abs_value.vhd
-- Descripcion:   Circuito que obtiene el valor absoluto de un numero.
-- Entradas:         x     datos
--                   ctrl  x: signo y magnitud si ctrl = 1
--                         x: CA2 si ctrl = 0
-- Salidas:          r     valor absoluto
--
-- Ejercicio:     8.13
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity abs_value is
   port  (  x     : in std_logic_vector(7 downto 0);
            ctrl  : in std_logic;
            y     : out std_logic_vector(7 downto 0)
          );
end abs_value;

architecture behavioral of abs_value is

   -- constants

   -- signals
   signal sig_x : unsigned(7 downto 0) := (others => '0');

begin

   sig_x <= not(unsigned(x));

   with ctrl select
      y  <= ('0' & x(6 downto 0))         when '0',
            std_logic_vector(sig_x + 1)   when others;

end behavioral;



