----------------------------------------------------------------------------------
-- Archivo:       tb_arith_unit2.vhd
-- Descripcion:   Testbench de: Circuito encargado de resolver operaciones 
--                aritmeticas con algunos flags.
--
-- Ejercicio:     8.15
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_arith_unit2 is
end tb_arith_unit2;
 
architecture behavior of tb_arith_unit2 is
 
    -- component declaration for the unit under test (uut)
    component arith_unit2
    port(
         a : in  std_logic_vector(7 downto 0);
         b : in  std_logic_vector(7 downto 0);
         op : in  std_logic_vector(1 downto 0);
         r : out  std_logic_vector(7 downto 0);
         co : out  std_logic;
         ov : out  std_logic;
         n : out  std_logic;
         z : out  std_logic
        );
    end component;
    

   --inputs
   signal a : std_logic_vector(7 downto 0) := (others => '0');
   signal b : std_logic_vector(7 downto 0) := (others => '0');
   signal op : std_logic_vector(1 downto 0) := (others => '0');

  --outputs
   signal r : std_logic_vector(7 downto 0);
   signal co : std_logic;
   signal ov : std_logic;
   signal n : std_logic;
   signal z : std_logic;

 
begin
 
  -- instantiate the unit under test (uut)
   uut: arith_unit2 port map (
          a => a,
          b => b,
          op => op,
          r => r,
          co => co,
          ov => ov,
          n => n,
          z => z
        );

   stim_proc: process
   begin
      a <= x"0F";
      b <= x"F0";
         op <= "00";
         wait for 20 ns;
         op <= "01";
         wait for 20 ns;
         op <= "10";
         wait for 20 ns;
         op <= "11";
         wait for 20 ns;

      a <= x"FF";
      b <= x"FF";
         op <= "00";
         wait for 20 ns;
         op <= "01";
         wait for 20 ns;
         op <= "10";
         wait for 20 ns;
         op <= "11";
         wait for 20 ns;
      
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



