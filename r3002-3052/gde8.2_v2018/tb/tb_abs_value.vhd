----------------------------------------------------------------------------------
-- Archivo:       tb_abs_value.vhd
-- Descripcion:   Testbench de: Circuito que obtiene el valor absoluto de un 
--                numero.
--
-- Ejercicio:     8.13
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_abs_value is
end tb_abs_value;
 
architecture behavior of tb_abs_value is
 
    -- component declaration for the unit under test (uut)
    component abs_value
    port(
         x : in  std_logic_vector(7 downto 0);
         ctrl : in  std_logic;
         y : out  std_logic_vector(7 downto 0)
        );
    end component;
    

   --inputs
   signal x : std_logic_vector(7 downto 0) := (others => '0');
   signal ctrl : std_logic := '0';

   --outputs
   signal y : std_logic_vector(7 downto 0);

 
begin
 
   -- instantiate the unit under test (uut)
   uut: abs_value port map (
          x => x,
          ctrl => ctrl,
          y => y
        );

   stim_proc: process
   begin
      x <= x"00";
         ctrl <= '0';
         wait for 20 ns;
         ctrl <= '1';
         wait for 20 ns;
      x <= x"0F";
         ctrl <= '0';
         wait for 20 ns;
         ctrl <= '1';
         wait for 20 ns;
      x <= x"F0";
         ctrl <= '0';
         wait for 20 ns;
         ctrl <= '1';
         wait for 20 ns;
      x <= x"FF";
         ctrl <= '0';
         wait for 20 ns;
         ctrl <= '1';
         wait for 20 ns;
      x <= x"A5";
         ctrl <= '0';
         wait for 20 ns;
         ctrl <= '1';
         wait for 20 ns;
      
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



