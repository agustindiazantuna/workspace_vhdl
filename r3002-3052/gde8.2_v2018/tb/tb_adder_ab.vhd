----------------------------------------------------------------------------------
-- Archivo:       tb_adder_ab.vhd
-- Descripcion:   Testbench de: Circuito sumador de dos entradas de datos.
--
-- Ejercicio:     8.11
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         22-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_adder_ab is
end tb_adder_ab;
 
architecture behavior of tb_adder_ab is
 
    -- component declaration for the unit under test (uut)
    component adder_ab
    port(
         a : in  std_logic_vector(15 downto 0);
         b : in  std_logic_vector(15 downto 0);
         r : out  std_logic_vector(15 downto 0)
        );
    end component;
    

   --inputs
   signal a : std_logic_vector(15 downto 0) := (others => '0');
   signal b : std_logic_vector(15 downto 0) := (others => '0');

   --outputs
   signal r : std_logic_vector(15 downto 0);

 
begin
 
   -- instantiate the unit under test (uut)
   uut: adder_ab port map (
          a => a,
          b => b,
          r => r
        );

   -- stimulus process
   stim_proc: process
   begin
      a <= x"0000";
      b <= x"00FF";
      wait for 20 ns;
      a <= x"FF00";
      b <= x"0000";
      wait for 20 ns;
      a <= x"A5A5";
      b <= x"5A5A";
      wait for 20 ns;
      a <= x"0000";
      b <= x"0000";
      wait for 20 ns;
      
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



