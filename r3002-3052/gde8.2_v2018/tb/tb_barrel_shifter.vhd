----------------------------------------------------------------------------------
-- Archivo:       tb_barrel_shifter.vhd
-- Descripcion:   Testbench de: Circuito que realiza desplazamientos.
--
-- Ejercicio:     8.14
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_barrel_shifter is
end tb_barrel_shifter;
 
architecture behavior of tb_barrel_shifter is
 
    -- component declaration for the unit under test (uut)
    component barrel_shifter
    port(
         x : in  std_logic_vector(7 downto 0);
         dir : in  std_logic;
         amt : in  std_logic_vector(2 downto 0);
         y : out  std_logic_vector(7 downto 0)
        );
    end component;
    

   --inputs
   signal x : std_logic_vector(7 downto 0) := (others => '0');
   signal dir : std_logic := '0';
   signal amt : std_logic_vector(2 downto 0) := (others => '0');

   --outputs
   signal y : std_logic_vector(7 downto 0);

 
begin
 
   -- instantiate the unit under test (uut)
   uut: barrel_shifter port map (
          x => x,
          dir => dir,
          amt => amt,
          y => y
        );

   stim_proc: process
   begin
      x <= x"A5";

      dir <= '0';
      amt <= "000";
      wait for 20 ns;
      amt <= "001";
      wait for 20 ns;
      amt <= "010";
      wait for 20 ns;
      amt <= "011";
      wait for 20 ns;
      amt <= "100";
      wait for 20 ns;
      amt <= "101";
      wait for 20 ns;
      amt <= "110";
      wait for 20 ns;
      amt <= "111";
      wait for 20 ns;

      dir <= '1';
      amt <= "000";
      wait for 20 ns;
      amt <= "001";
      wait for 20 ns;
      amt <= "010";
      wait for 20 ns;
      amt <= "011";
      wait for 20 ns;
      amt <= "100";
      wait for 20 ns;
      amt <= "101";
      wait for 20 ns;
      amt <= "110";
      wait for 20 ns;
      amt <= "111";
      wait for 20 ns;

    report "Simulation completed";
    assert false severity failure;
   end process;

end;



