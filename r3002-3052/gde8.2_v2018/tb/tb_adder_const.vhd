----------------------------------------------------------------------------------
-- Archivo:       tb_adder_const.vhd
-- Descripcion:   Testbench de: Circuito sumador de un dato con una constante.
--
-- Ejercicio:     8.11
-- Guia:          v1.0
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-09-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_adder_const is
end tb_adder_const;
 
architecture behavior of tb_adder_const is
 
    -- component declaration for the unit under test (uut)
    component adder_const
    port(
         a : in  std_logic_vector(15 downto 0);
         r : out  std_logic_vector(15 downto 0)
        );
    end component;
    

   --inputs
   signal a : std_logic_vector(15 downto 0) := (others => '0');

   --outputs
   signal r : std_logic_vector(15 downto 0);

 
begin
 
   -- instantiate the unit under test (uut)
   uut: adder_const port map (
          a => a,
          r => r
        );

   -- stimulus process
   stim_proc: process
   begin
      a <= x"0000";
      wait for 20 ns;
      a <= x"00FF";
      wait for 20 ns;
      a <= x"A5A5";
      wait for 20 ns;
      a <= x"FFFF";
      wait for 20 ns;
      
    report "Simulation completed";
    assert false severity failure;
   end process;

end;



