----------------------------------------------------------------------------------
-- Archivo:       tb_ffd_en.vhd
-- Descripcion:   Testbench de: Registro de Flip-Flop D de longitud configurable 
--                y enable.
--
-- Ejercicio:     TPI
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-10-2018
----------------------------------------------------------------------------------


----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
---------------------------------------------------------------------------------- 
entity tb_ffd_en is
end tb_ffd_en;
 
architecture behavior of tb_ffd_en is 
 
    -- component declaration for the unit under test (uut)
    component ffd_en
    port(
         d : in  std_logic_vector(15 downto 0);
         clk : in  std_logic;
         rst : in  std_logic;
         en : in  std_logic;
         q : out  std_logic_vector(15 downto 0)
        );
    end component;

   --inputs
   signal d : std_logic_vector(15 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal en : std_logic := '0';

  --outputs
   signal q : std_logic_vector(15 downto 0);

   -- clock period definitions
   constant clk_period : time := 10 ns;
 
begin
 
  -- instantiate the unit under test (uut)
   uut: ffd_en port map (
          d => d,
          clk => clk,
          rst => rst,
          en => en,
          q => q
        );

   -- clock process definitions
   clk_process :process
   begin
    clk <= '1';
    wait for clk_period/2;
    clk <= '0';
    wait for clk_period/2;
   end process;
 

   -- stimulus process
   stim_proc: process
   begin    
      rst <= '1';
      en <= '0';
      d <= x"0F5A";
      wait for clk_period*2;

      rst <= '0';
      wait for clk_period*2;

      en <= '1';
      wait for clk_period*2;

      d <= x"A5F0";
      wait for clk_period*1.5;
      d <= x"3333";
      wait for clk_period*1.5;
      en <= '0';
      d <= x"FFFF";
      wait for clk_period;

      report "Simulation completed";
      assert false severity failure;
   end process;

end;



