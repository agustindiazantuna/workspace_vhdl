LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use ieee.std_logic_textio.all;
USE std.textio.all;
 
ENTITY tb_fir_transpose IS
  generic(
    W : natural := 16
  );
END tb_fir_transpose;
 
ARCHITECTURE behavior OF tb_fir_transpose IS 

  --Inputs
  signal test_clk: std_logic := '0';
  signal test_rst: std_logic := '0';
  signal test_en : std_logic := '1';
  signal test_x  : std_logic_vector(W-1 downto 0) := (others => '0');
  --Outputs
  signal test_y:   std_logic_vector(W-1 downto 0);
  -- Clock period definitions
  constant clk_period : time := 20.8333 us;
  constant SYN_DELAY:   time := 0.25*clk_period;

  FILE sgnl_file: TEXT OPEN READ_MODE IS "./tb/sgnl_file.txt";
  FILE gldn_file: TEXT OPEN READ_MODE IS "./tb/gldn_file.txt";
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
  uut: entity work.fir_transpose
  generic map(
    L  => 32,
    W  => 16
  )
  port map(
    x   => test_x, 
    clk => test_clk, 
    rst => test_rst,
    en  => test_en,
    y   => test_y
  );
	

  -- Clock process definitions
  clk_process: process
  begin
    test_clk <= '0';
    wait for clk_period/2;
    test_clk <= '1';
    wait for clk_period/2;
  end process clk_process;

  stim_proc: process
    variable aux_line   : line;
    variable sgnl_value : std_logic_vector(W-1 downto 0);
    variable gldn_value : std_logic_vector(W-1 downto 0);
    variable errors     : integer := 0;
  begin
    test_rst <= '0';
    wait for clk_period * 4 + SYN_DELAY;
    test_rst <= '1';
    wait for clk_period;
    test_rst <= '0';

    while (not endfile(sgnl_file)) loop
      readline(sgnl_file, aux_line);
      read(aux_line, sgnl_value);
      test_x <= sgnl_value;
      wait for clk_period;
      readline(gldn_file, aux_line);
      read(aux_line, gldn_value);
      if (test_y /= gldn_value) then
        errors := errors + 1;
        report "!!! MISMATCH ERROR !!!" & LF &
               "Expected value = " & INTEGER'IMAGE(to_integer(unsigned(gldn_value))) & LF &
               "Current  value = " & INTEGER'IMAGE(to_integer(unsigned(test_y)))     & LF & LF    
        severity warning;
      end if;
    end loop;

    if (errors = 0) then
      report LF & 
      "***************************************" & LF & 
      "   NO ERRORS FOUND    SUCCESSFUL SIM   " & LF &
      "***************************************" & LF & LF;
    else
      report LF & 
      "***************************************" & LF & 
      "   FAILED TEST          ERRORS FOUND   " & LF & 
      "***************************************" & LF & LF;
    end if;
    
    assert false 
      report LF & 
      "***************************************" & LF &
      "           SIMULATION   ENDS           " & LF &
      "***************************************" & LF & LF
      severity failure;
  end process stim_proc;

END;
