----------------------------------------------------------------------------------
-- Archivo:       ffd_en.vhd
-- Descripcion:   Registro de Flip-Flop D de longitud configurable y enable.
-- Generic:          W     longitud del registro
-- Entradas:         d     entrada (W bits)
--                   clk   clock input
--                   rst   reset async with '1'
--                   en    enable with '1'
-- Salidas:          q     salida (W bits)
--
-- Ejercicio:     TPI
-- Anio:          2018
--
-- Autor:         Agustin Diaz Antuna
-- Contacto:      agustin.diazantuna@gmail.com
-- Fecha:         23-10-2018
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
-- library
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;


----------------------------------------------------------------------------------
-- entity
----------------------------------------------------------------------------------
entity ffd_en is
   generic ( W    : natural := 16 );
   port  (  d     : in std_logic_vector(W-1 downto 0);
            clk   : in std_logic;
            rst   : in std_logic;
            en    : in std_logic;
            q     : out std_logic_vector(W-1 downto 0)
          );
end ffd_en;

architecture behavioral of ffd_en is

   -- constants

   -- signals

begin

   -- memory
   memory: process(clk, rst)
   begin
      if(rst = '1') then
         q <= (others => '0');
      elsif(rising_edge(clk)) then
         if(en = '1') then
            q <= d;
         end if;
      end if;
   end process memory;


end behavioral;



