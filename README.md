- - -

Workspace de ejercicios/proyectos descriptos en VHDL
====================

Las guías de ejercicios resueltas corresponden a cursos de la materia **Técnicas Digitales I** pertenecientes a la carrera **Ingeniería Electrónica** dictada en la **Universidad Tecnológica Nacional (UTN)** **Facultad Regional Buenos Aires (FRBA)**.

- - -

# Curso r3002-3052

Incluye los ejercicios propuestos para los cursos *R3002* y *R3052*.

Dentro de este directorio se pueden encontrar:

+ Guía de ejercicios con todos los enunciados
+ Código fuente
+ Testbench



# My Projects

Proyectos desarrollados fuera de la cátedra.



## FIR filter

Implementación de un filtro FIR de largo de palabra y cantidad de etapas configurables al momento de instanciar el circuito.

Los componentes son almacenados en una memoria ROM con los datos obtenidos de archivos *.coe

El filtro opera con aritmética de punto fijo, utiliza un único multiplicador y acumulador.

El tiempo de respuesta es de L ciclos de clock, siendo L la cantidad de etapas.

+ Board: **Atlys** (Digilent)
+ FPGA: **Spartan 6** (Xilinx)
   * Modelo: XC6SLX45
   * Encapsulado: CSG324C
+ Audio codec: LM4550 AC'97 (National Semiconductor)

Debe ser probado sobre el hardware indicado anteriormente.

Modo de uso:

+ Audio INPUT: Jack LINE IN
+ Audio OUTPUT: Jack HP OUT
+ Reset: Reset del circuito



## Loopback Atlys Spartan 6

Este proyecto provee un sencillo loopback utilizando un IP-Core adaptado de los fuentes de un tercero.

+ Board: **Atlys** (Digilent)
+ FPGA: **Spartan 6** (Xilinx)
   * Modelo: XC6SLX45
   * Encapsulado: CSG324C
+ Audio codec: LM4550 AC'97 (National Semiconductor)

Debe ser probado sobre el hardware indicado anteriormente.

Modo de uso:

+ Audio INPUT: Jack LINE IN
+ Audio OUTPUT: Jack HP OUT
+ SW7: Entrada de modo, selecciona *loopback* o *algoritmo propio* (aquí se debe agregar el circuito de procesamiento deseado)
+ SW4 - SW0: Control de volumen
+ Reset: Reset del circuito




***

Contacto:

+ agustin.diazantuna@gmail.com

***
